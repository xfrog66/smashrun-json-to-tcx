const fs = require('fs');
const path = require('path');
const files = fs.readdirSync('./json');


const fileTpl = fs.readFileSync('scripts/template-file.xml').toString();
const pointTpl = fs.readFileSync('scripts/template-trackpoint.xml').toString();


for (const fileName of files) {
    console.log('>>> parsing ', fileName);
    
    const nk = JSON.parse(fs.readFileSync(path.join('./json', fileName)));
    


    let startDate = new Date(nk.start_epoch_ms).toISOString();
    let totalTime = nk.active_duration_ms / 1000;
    let totalDistance = nk.summaries.find((e) => e.metric === 'distance').value * 1000;
    let totalCalories = nk.summaries.find((e) => e.metric === 'calories').value;


    let notes = '';
    notes += 'Température: ' + nk.tags['com.nike.temperature'];
    


    const distances = nk.metrics.find((e) => e.type === 'distance').values;
    const longitudes = nk.metrics.find((e) => e.type === 'longitude').values;
    const latitudes = nk.metrics.find((e) => e.type === 'latitude').values;
    const altitudes = nk.metrics.find((e) => e.type === 'elevation').values;


    const xmlTrackPoints = [];


    for (const object of longitudes) {
        try {

        
        const date = new Date(object.start_epoch_ms).toISOString();
        const longitude = object.value;
        const latitude = latitudes.find((e) => e.start_epoch_ms === object.start_epoch_ms).value;

        let altitude = -1;
        for (const iterator of altitudes) {
            if (iterator.start_epoch_ms > object.start_epoch_ms) {
                break;
            }

            altitude = iterator.value;

        }

        let distance = 0;
        for (const iterator of distances) {
            if (iterator.start_epoch_ms <= object.start_epoch_ms) {
                distance += (iterator.value * 1000);
            }
            else {
                break;
            }
        }
        
        if (altitude > 0) {
            xmlTrackPoints.push(
                pointTpl.replace(/%startDate%/g, date)
                        .replace('%latitude%', latitude)
                        .replace('%longitude%', longitude)
                        .replace('%altitude%', altitude)
                        .replace('%distance%', distance));
        }

        
        
        }
        catch (e) {
            console.log('problem with the trackpoint ', object.start_epoch_ms);
            
        }
    }


    let xmlContent = fileTpl.replace(/%startDate%/g, startDate)
        .replace('%totalTime%', totalTime)
        .replace('%totalDistance%', totalDistance)
        .replace('%totalCalories%', totalCalories)
        .replace('%notes%', notes)
        .replace('%trackpoint%', xmlTrackPoints.join(''));
    
    

    fs.writeFileSync(path.join('./tcx', fileName.substring(0, fileName.length - 5) + '.tcx'), xmlContent);


}


