# Smashrun json to tcx converter
I wanted to be able to export data of my Nike+ account into Strava. Following some guide I used Smash run to perform the synchronization.
However I had the bad surprise that importing from Smashrun into Strava (or any running app) is chargeable whereas the raw data export is not.

But raw output cannot be imported back into Strava because this is a propretiary structure.
The purpose of this app is to provide a free of charge way to convert the files into TCX file which can be imported into running app (at least Strava)

Be a bit more manual, but free of charge and it worked for me.


## Process
In order to be able to export data from Nike plus to Strava (or any app supporting tcx files), for free let's do like this:


1. Subscribe to Smashrun and sync it with Nike+
2. Export all the runs to Dropbox (which is free, exporting directly to Strava is chargeable)
3. Download all the zip files present in dropbox to your PC and extract them
4. Keep only the right json file and copy then into the `json` folder
5. Run the tool `npm run convert`. This will create one tcx file per json file
6. Connect to strava and import all of them


## Dependencies
Just require node
